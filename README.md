# Blog Gybrán Pérez
###### Esta aplicación fue creada siguiendo el libro ["Getting MEAN with Mongo, Express, Angular, and Node, Second Edition"](https://learning.oreilly.com/library/view/getting-mean-with/9781617294754) 

![Arquitectura](./public/images/getting-mean/arquitectura-flujo.jpg)

**Figura 1.** Arquitectura general del proyecto 

### Ejecutar la app de forma local
#### Instalar dependencias:
```
npm install
```
#### Ejecutar localmente:
Usar `npm start` si se quiere ejecutar sin hacer pruebas, sino usar `nodemon`

### Actualizar Repositorio 
Para conocer el funcionamiento de la herramienta Git visitar el siguiente [link](https://rogerdudler.github.io/git-guide/index.es.html)
#### GITLAB
```
git add .
git commit -m "actualizado"
git push origin main
```

#### HEROKU
Primero debemos instalar Heroku y acceder con nuestra cuenta con `heroku login`, después en la carpeta del proyecto corremos el siguiente comando:
```
heroku create
```
La salida del comando anterior será algo parecido a esto:
```
Creating app... done, ⬢ ALGUN-PATRON
https://ALGUN-PATRON.herokuapp.com/ | https://git.heroku.com/ALGUN-PATRON.git
```

Ahora podremos ver en nuestro [Dashboard](https://dashboard.heroku.com) de Heroku nuestro nuevo repositorio, lo que sigue es añadir una rama local de heroku y enlazarla con el repositorio: 
```
heroku git:remote -a ALGUN-PATRON
```

Por último subiremos al repositorio.
```
git add .
git commit -m "actualizado"
git push heroku main
```