var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');


const appServer = path.join(__dirname, "app_server");

//EXPRESS
var app = express();
//   APP SERVER
//      ROUTES
const routesServer = path.join(appServer, "routes");
const indexRouter = require(path.join(routesServer, 'index'));
const usersRouter = require(path.join(routesServer, 'users'));
const emailRouter = require(path.join(routesServer,'email'));
const jobsRouter = require(path.join(routesServer, 'jobs'));
//      ROUTERS
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/email', emailRouter);
app.use('/jobs', jobsRouter);
//      VIEWS
const viewsServer = path.join(appServer, "views");
app.set('views', viewsServer);
//      VIEWS ENGINE
app.set('view engine', 'pug');
//      RESOURCES
app.use("/bootstrap", express.static(path.join(__dirname, '/node_modules/bootstrap/dist/')));
app.use("/popper", express.static(path.join(__dirname, '/node_modules/@popperjs/core/dist/umd/')));
app.use("/font-icons", express.static(path.join(__dirname, '/node_modules/@fortawesome/fontawesome-free/')));
//      STATIC FILES
app.use(express.static(path.join(__dirname, 'public')));

//  APP API



// CONFIG
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});
// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;