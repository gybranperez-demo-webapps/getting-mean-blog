var express = require('express');
var router = express.Router();
// CONTROLLERS
const jobContr = require('../controllers/jobs'); 

// ROUTES
router.get('/', jobContr.list);
router.route('/:job_id')
  .all(function (req, res, next) {
    // Todas las peticiones con ese patron pasan por aqui
    // Es el middleware de la ruta
    next()
  })
  .get(jobContr.get)
  .put(jobContr.update)
  .post(jobContr.create)
  .delete(jobContr.remove)

// EXPORT ROUTES

module.exports = router;