var express = require('express');
var router = express.Router();

// CONTROLLERS
const usersContr = require('../controllers/users'); 

// ROUTES
router.route('/:user_id')
  .all(function (req, res, next) {
    // Todas las peticiones con ese patron pasan por aqui
    // Es el middleware de la ruta
    next()
  })
  .get(usersContr.get)
  .put(usersContr.update)
  .post(usersContr.create)
  .delete(usersContr.remove)

module.exports = router;
