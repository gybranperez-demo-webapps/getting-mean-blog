var express = require('express');
var router = express.Router();

/* GET users listing. */
const emailContr = require('../controllers/email'); 
// ROUTES
router.route('/send')
  .all(function (req, res, next) {
    // Todas las peticiones con ese patron pasan por aqui
    // Es el middleware de la ruta
    next()
  })
  .get(emailContr.formEmail)
  .post(emailContr.sendEmail)
module.exports = router;