var express = require('express');
var router = express.Router();
// CONTROLLERS
const mainContr = require('../controllers/main'); 
// ROUTES
router.get('/', mainContr.index);
router.get('/interests', mainContr.interests);
router.get('/cv/tech', mainContr.techTable);
// EXPORT ROUTES
module.exports = router;
