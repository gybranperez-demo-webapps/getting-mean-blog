/* GET homepage */
const index = (req, res) => {
    const user = {
      email:"gybranperez@gmail.com",
      nickname: "gybranperez",
      phone:"+525585468932",
      bday: "1998-4-26",
      location: "Mexico City",
      occupation:"Full Stack Developer",
      mainBeliefs: "Tengo la firme creencia de que entre más formas tengas de ver un problema, más cerca estarás de la solución; esto me ha impulsado a expandir mis horizontes, estudiar nuevos lenguajes de programación, revisar frameworks que me apoyen a realizar tareas de forma automatizada o faciliten la misma, con el fin de entregar los resultados esperados. Adquiriendo estos conocimientos, se puede dar una solución más precisa sobre las tecnologías que podrían utilizar en un proyecto con ciertos requerimientos.Este último año me he dedicado en su mayoría a 'afilar la sierra' como se menciona en el libro 7 hábitos de la gente altamente efectiva con el propósito de superarme y poder dar lo mejor de mi en todos los ámbitos de mi vida.",
      jobShortDescription: "I enjoy coding and try new technologies in the process. In the last two years I've been learning and developing with Angular, Django and ExpressJS.",
      principles:["Empatía","Persistencia","Honestidad","Familia"],
      linktree: "https://linktr.ee/gybranperez/",
      github:"https://github.com/gybranperez/",
      education: [
        {
          "institute": "Escuela Superior de Cómputo (ESCOM) - IPN ",
          "name": "Ingeniería en sistemas Computacionales",
          "degree": "bachelor",
          "from": 2016,
          "to": "Current"
        }
      ],
      languages: [
        { name: "spanish", rating: 5 },
        { name: "english", rating: 4 }
      ]
    };
    let technologies = [
      {
        "category": "Frontend",
        "list":["Angular", "Bootstrap","materializecss","HTML5", "CSS3", "JS", "Jquery"]
      },{
        "category": "Backend",
        "list":["Python 3", "Django 3", "ExpressJS", "Java 11", "REST"]
      },
      {
        "category": "Microservices",
        "list":["MoleculerJS", "Django Rest Framework", "Flask", "Spring Boot"]
      },{
        "category": "Kubernetes Architecture",
        "list":["Rancher", "Docker", "Ficheros yaml", "Ficheros Dockerfile", "Pipeline"]
      },{
        "category": "Servers",
        "list":["Apache 2", "Tomcat 9", "Nginx", "Glassfish"]
      },{
        "category": "IDEs",
        "list":["Eclipse", "STS", "VS Code", "Netbeans"]
      },{
        "category": "Tools",
        "list":["Git", "heroku", "Dockerhub", "npm", "Postman", "Maven","Swagger"]
      },
    ];      
    user.mainBeliefs = user.mainBeliefs.split(".");
    let timeline = [
      {
          "id": 1,
          "category": "graphic",
          "jobs": ["Diseñador Gráfico","Diseñador Web"],
          "start": "2014-9-1",
          "end": "2014-12-1",
          "title": "Geonix",
          "name": "Diseño Geonix",
          "description":""
        }, {
          "id": 3,
          "category": "web",
          "jobs": ["Diseñador Web","Diseñador Gráfico"],
          "start": "2015-8-1",
          "end": "2015-12-1",
          "title": "Colaycloc",
          "name": "",
          "description":""
        }, {
          "id": 4,
          "category": "products",
          "jobs": ["Diseñador de productos","Imprensión en Serigrafía","Diseñador Gráfico"],
          "start": "2015-10-1",
          "end": "2016-5-1",
          "title": "Two Points V",
          "name": "",
          "description":""
        }, {
          "id": 5,
          "category": "web",
          "jobs": ["Diseñador Web","Diseño de identidad corporativa","Diseño de artículos promocionales"],
          "start": "2016-3-1",
          "end": "2016-7-1",
          "title": "Dasti",
          "name": "",
          "description":""
        },{
          "id": 6,
          "category": "web",
          "jobs": ["Diseñador Web"],
          "start": "2016-2-1",
          "end": "2016-4-1",
          "title": "Orion",
          "name": "",
          "description":""
        }, {
          "id": 7,
          "category": "web",
          "jobs": ["Asesoría en Diseño Web"],
          "start": "2016-2-1",
          "end": "2016-6-1",
          "title": "Yo soy Poli",
          "name": "",
          "description":""
        }, {
          "id": 8,
          "category": "web",
          "start": "2019-3-1",
          "jobs": ["Desarrollador Web Frontend"],
          "end": "2019-9-1",
          "title": "Web KIO",
          "name": "",
          "description":""
        }, {
          "id": 9,
          "category": "web",
          "start": "2019-11-1",
          "jobs": ["Desarrollador Web Fullstack"],
          "end": "2020-4-1",
          "title": "CI/CD KIO",
          "name": "",
          "description":""
        }, {
          "id": 10,
          "category": "web",
          "start": "2021-6-1",
          "jobs": ["Desarrollador Web Fullstack"],
          "end": "Current",
          "title": "Express Angular",
          "name": "",
          "description":""
        }, 
        /*
        {
          "id": 2,
          "category": "web",
          "jobs": ["Desarrollador Web Fullstack"],
          "start": "2021-9-1",
          "end": "Current",
          "title": "Servicio Social",
          "name": "",
          "description":""
        }, {
          "id": 10,
          "category": "web",
          "jobs": ["Desarrollador Web Fullstack"],
          "start": "2021-8-1",
          "end": "Current",
          "title": "Trabajo Terminal",
          "name": "",
          "description":""
        }*/
    ]
    timeline.forEach(function(job) {
        if(job.end != "Current"){
            job.end = new Date(job.end);
        }
        job.start = new Date(job.start);
      });
    
    timeline.sort((a, b) => b.start - a.start);
    const context = { 
      title: 'CV',
      user: user,
      history: timeline,
      technologies: technologies,
    }
    res.render('index', context); 
};
const techTable = (req, res) => {      
  let technologies = [
    {
      "category": "Frontend",
      "list":["Angular", "Bootstrap","materializecss","HTML5", "CSS3", "JS", "Jquery"]
    },{
      "category": "Backend",
      "list":["Python 3", "Django 3", "ExpressJS", "REST"]
    },
    {
      "category": "Microservices",
      "list":["MoleculerJS", "Django Rest Framework", "Flask", "Spring Boot"]
    },{
      "category": "Kubernetes Architecture",
      "list":["Rancher", "Docker", "Ficheros yaml", "Ficheros Dockerfile", "Pipeline"]
    },{
      "category": "Servers",
      "list":["Apache 2", "Tomcat 9", "Nginx", "Glassfish"]
    },{
      "category": "IDEs",
      "list":["Eclipse", "STS", "VS Code", "Netbeans"]
    },{
      "category": "Tools",
      "list":["Git", "heroku", "npm", "Gradle", "Swagger"]
    },
  ];   
    res.render('includes/index/technologies', { title: 'Technologies', technologies:technologies }); 
};
const interests = (req, res) => {                
    res.render('interests', { title: 'Interests' }); 
};
module.exports = {                           
    index,
    techTable,
    interests
};   