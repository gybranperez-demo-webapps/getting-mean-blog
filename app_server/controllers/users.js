const get = (req, res, next) => {
    let user = req.params.user_id;      
    res.render('index', { title: ('Usuario: ' + user) }); 
};
const create = (req, res, next) => {                
    res.send('Crear Usuario');
};
const remove = (req, res, next) => {                
    res.send('Borrar Usuario');
};
const update = (req, res, next) => {                
    res.send('Actualizar un Usuario');
};
module.exports = {                           
    get, create, update, remove
};   