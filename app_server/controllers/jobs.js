const createError = require('http-errors');
const timeline =   [
  {
      "id": 1,
      "category": "graphic",
      "jobs": ["Diseñador Gráfico","Diseñador Web"],
      "start": "2014-9-1",
      "end": "2014-12-1",
      "title": "Geonix",
      "name": "Diseño Geonix",
      "description":""
    },  {
      "id": 3,
      "category": "web",
      "jobs": ["Diseñador Web","Diseñador Gráfico"],
      "start": "2015-8-1",
      "end": "2015-12-1",
      "title": "Colaycloc",
      "name": "",
      "description":""
    }, {
      "id": 4,
      "category": "products",
      "jobs": ["Diseñador de productos","Imprensión en Serigrafía","Diseñador Gráfico"],
      "start": "2015-10-1",
      "end": "2016-5-1",
      "title": "Two Points V",
      "name": "",
      "description":""
    }, {
      "id": 5,
      "category": "web",
      "jobs": ["Diseñador Web"],
      "start": "2016-2-1",
      "end": "2016-4-1",
      "title": "Orion",
      "name": "",
      "description":""
    }, {
      "id": 6,
      "category": "web",
      "jobs": ["Asesoría en Diseño Web"],
      "start": "2016-2-1",
      "end": "2016-6-1",
      "title": "Yo soy Poli",
      "name": "",
      "description":""
    }, {
      "id": 7,
      "category": "web",
      "start": "2019-3-1",
      "jobs": ["Desarrollador Web"],
      "end": "2019-9-1",
      "title": "Web KIO",
      "name": "",
      "description":""
    }, {
      "id": 8,
      "category": "web",
      "start": "2019-11-1",
      "jobs": ["Desarrollador Web"],
      "end": "2020-4-1",
      "title": "CD CI KIO",
      "name": "",
      "description":""
    }, {
      "id": 9,
      "category": "web",
      "start": "2021-6-1",
      "jobs": ["Desarrollador Web Fullstack"],
      "end": "Current",
      "title": "Express Angular",
      "name": "",
      "description":""
    },
    /*
    {
      "id": 2,
      "category": "web",
      "jobs": ["Desarrollador Web Fullstack"],
      "start": "2021-9-1",
      "end": "Current",
      "title": "Servicio Social",
      "name": "",
      "description":""
    }, {
      "id": 10,
      "category": "web",
      "jobs": ["Desarrollador Web Fullstack"],
      "start": "2021-8-1",
      "end": "Current",
      "title": "Trabajo Terminal",
      "name": "",
      "description":""
    }
    */
]
timeline.forEach(function(job) {
  if(job.end != "Current"){
      job.end = new Date(job.end);
  }
  job.start = new Date(job.start);
});
const list = (req, res, next) => {  
    // fake posts to simulate a database
    res.render('jobs/list', { title:'Trabajos', jobList:timeline  });
};
const get = (req, res, next) => {   
    let job = req.params.job_id; 
    const user = timeline.find(timeline => timeline.id == job);
    if(user){     
      res.render('jobs/detail', { title: ('Detalle Trabajo ' + user), job: user  }); 
    } else{
      next(createError(404));
    }
};
const create = (req, res, next) => {                
    res.send('Crear trabajo');
};
const remove = (req, res, next) => {                
    res.send('Borrar trabajo');
};
const update = (req, res, next) => {                
    res.send('Actualizar un trabajo');
};
module.exports = {                           
    get, create, update, remove, list
};   