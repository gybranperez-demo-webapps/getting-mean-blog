function jobDateFormatter(list) {
  list.forEach(function(job) {
    if(job.end != "Current"){
        job.end = new Date(job.end);
    }
    job.start = new Date(job.start);
  });
}
